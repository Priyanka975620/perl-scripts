#! /usr/bin/perl

# This script is used to parse any input file and search for list of keywords and also to ignore few
# search strings.This is a good way to see if there are any warnings, exceptions in a log file,etc

use strict;
use warnings;

my $msg_ref;

&parse_log (
   Log => "Log.txt",
   Msg => \$msg_ref,
   );

 print $msg_ref;

sub parse_log {

   my %params = @_;
   my $line_num;
   my $log = $params{Log};
   my $msg_ref = $params{Msg};
   my $text;
   my $msg = "Interesting log information follows:\n";
   my $line;
   my %error_strings;

   # using fw common fail strings in any log
   my @LOG_STRINGS = (
      "permission denied",            # permission problems
      "no space",                     # ran out of space filesystem
      "not responsive",
      "fail",
      "warning",
      "error"
   );
   my @exclude_strings = (
      "WARNING: Its not a critical failure, hence ignored"
   );
   # my $key;
   my $set_num = 1;
   my $exclude = 0;

   if(!-e $log) {
      print ("\n log file doesnot exist");
   }

   if(!-f $log) {
      print ("\nunable to open log file");
   }
   

   else {
      open(my $input_file,'<', $log);
      while ($line = <$input_file>) {
         $line_num++;
         $exclude = 0;
         foreach my $string (@LOG_STRINGS) {
            
            
            if ( $line =~ /$string/i ) {
               foreach my $exclude_string (@exclude_strings) {
                  if($line =~ /$exclude_string/i ) {
                     $exclude = 1;
                     last;
                  }
               }
               if(!$exclude) {
                $error_strings{$line_num} = $line;
             }
            }
            
         }
      }
      if (%error_strings) {
      foreach my $key (sort keys %error_strings) {
         $text = $error_strings{$key};
         $text =~ s/^\s+//g;
         $msg .= "Line:$key :: $text";
      }
   }
   else {
      $msg = " ";
   }
   $$msg_ref = $msg;
   }
}





