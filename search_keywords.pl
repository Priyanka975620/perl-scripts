# !usr/bin/perl
# This script is used to search a keyword giving any directory as the command line
# parameter where you want to search for it. Example taken is a simple regex(my search keyword) but 
# can be customised as required to search keywords or specific patterns in the file.


use strict;

use File::Find;
use File::Slurp;

my $output1 = "summary.txt";
my $output2 = "details.txt";
my $start_dir = shift;
my $file_count = 0;
my $flag;
my $flag1 = 0;

my @all_lines;
my @files;
my @lines;
my @line_numbers;

# Push every file in directory structure onto the array @files.
find (
  sub { push @files, $File::Find::name if !-d; },
  $start_dir
);

open (my $fh1, '>', $output2) or ("Could not open file '$output2' $!");

print $fh1 ("\n Files checked :");
foreach my $file (@files) {
    if($file =~ /.txt/){
        $file_count++;
        print $fh1 ("\n $file");
    }
}

print ("\n\nChecking $file_count files\n\n");

open(my $fh, '>', $output1) or die("Could not open file '$output1' $!");

foreach my $file (@files) {

    $flag = 0;

    if($file =~ /.txt/){

        @all_lines = read_file($file);

        for (my $i = 0; $i < scalar(@all_lines); $i++) {
            my $line = $all_lines[$i];
            if ($line =~ /^my search keyword/) {
                print $fh ("\n***Line matched at line: $i in the file: $file\n");
                $flag = 1;
                next;
            }
        }

        if($flag) {
        	$flag1++;
            print $fh1 ("\n\n$file found with the keyword being searched\n");
        }
    }
}

print $fh ("\n\n***********************************  SUMMARY  *****************************************************\n");

print $fh ("\nNo. of files checked = $file_count \n");

print $fh ("\nNo. of files in which keyword is found = $flag1 \n");

print $fh ("\n*****************************************************************************************************\n");

close($fh1);

close($fh);

print("Done!!!\n");
